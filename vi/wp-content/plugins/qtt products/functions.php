<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 3:18 PM
 * File: add-product.php
 * Support : thangnm@lms.vn
 */
?>

<?php
/**
 *Create Product post
 */

add_theme_support( 'post-thumbnails' );
add_action('init', 'create_product_post_type');
function create_product_post_type(){
    register_post_type('product',
        array(
            'labels'  =>  array(
                'name'  =>  __('Product'),
                'singular_name' =>  __('Product'),
                'add_new' =>  __('Add New'),
                'add_new_item'  =>  __('Add New Product'),
                'edit'  =>  __('Edit'),
                'edit_item' =>  __('Edit Product'),
                'new_item'  =>  __('New Product'),
                'view'  =>  __('View Product'),
                'view_item' =>  __('View Product'),
                'search_items' =>  __('Search Products'),
                'not_found' =>  __('No Products found'),
                'not_found_in_trash'  =>  __('No Products found in Trash')
            ),
            'public'  =>  true,
            'show_ui' =>  true,
            'publicy_queryable' =>  true,
            'exclude_from_search' =>  false,
            'menu_position' => 4,
//            'menu_icon' =>  get_stylesheet_directory_uri(). '/images/product.png',
            'hierarchical'  => false,
            'query_var' =>  true,
            'supports'  =>  array(
                'title', 'editor', 'thumbnail'
            ),
            'rewrite' =>  array('slug'  =>  'product', 'with_front' =>  false),
//            'taxonomies' =>  array('post_tag', 'category'),
            'can_export'  =>  true,
//            'register_meta_box_cb'  =>  'call_to_function_do_something',
            'description' =>  __('Product description here.')
        )
    );
}

add_action('init', 'create_product_taxonomies');
function  create_product_taxonomies(){
    register_taxonomy('pro_cate', 'product', array(
        'hierarchical'  =>  true,
        'labels'  =>  array(
            'name'  =>  __('Categories'),
            'singular_name' =>  __('ProCategories'),
            'add_new' =>  __('Add New'),
            'add_new_item'  =>  __('Add New Categories'),
            'new_item'  =>  __('New Categories'),
            'search_items' =>  __('Search Categories'),
        ),
    ));
}

/**
 *Create Technicality post
 */

add_action('init', 'create_tech_post_type');

function create_tech_post_type(){
    register_post_type('technicality',
        array(
            'labels'  =>  array(
                'name'  =>  __('Technicality'),
                'singular_name' =>  __('Technicality'),
                'add_new' =>  __('Add New'),
                'add_new_item'  =>  __('Add New Technicality'),
                'edit'  =>  __('Edit'),
                'edit_item' =>  __('Edit Technicality'),
                'new_item'  =>  __('New Technicality'),
                'view'  =>  __('View Technicality'),
                'view_item' =>  __('View Technicality'),
                'search_items' =>  __('Search Technicality'),
                'not_found' =>  __('No Technicality found'),
                'not_found_in_trash'  =>  __('No Technicality found in Trash')
            ),
            'public'  =>  true,
            'show_ui' =>  true,
            'publicy_queryable' =>  true,
            'exclude_from_search' =>  false,
            'menu_position' => 4,
//            'menu_icon' =>  get_stylesheet_directory_uri(). '/images/product.png',
            'hierarchical'  => false,
            'query_var' =>  true,
            'supports'  =>  array(
                'title', 'editor', 'thumbnail'
            ),
            'rewrite' =>  array('slug'  =>  'technicality', 'with_front' =>  false),
//            'taxonomies' =>  array('post_tag', 'category'),
            'can_export'  =>  true,
//            'register_meta_box_cb'  =>  'call_to_function_do_something',
            'description' =>  __('Technicality description here.')
        )
    );
}

add_action('init', 'create_tech_taxonomies');
function  create_tech_taxonomies(){
    register_taxonomy('tech_cate', 'technicality', array(
        'hierarchical'  =>  true,
        'labels'  =>  array(
            'name'  =>  __('Categories'),
            'singular_name' =>  __('TechCategories'),
            'add_new' =>  __('Add New'),
            'add_new_item'  =>  __('Add New Categories'),
            'new_item'  =>  __('New Categories'),
            'search_items' =>  __('Search Categories'),
        ),
    ));
}

/**
 *Create Help post
 */

add_action('init', 'create_help_post_type');
function create_help_post_type(){
    register_post_type('help',
        array(
            'labels'  =>  array(
                'name'  =>  __('Help'),
                'singular_name' =>  __('Help'),
                'add_new' =>  __('Add Help'),
                'add_new_item'  =>  __('Add New Help'),
                'edit'  =>  __('Edit'),
                'edit_item' =>  __('Edit Help'),
                'new_item'  =>  __('New Help'),
                'view'  =>  __('View Help'),
                'view_item' =>  __('View Help'),
                'search_items' =>  __('Search Help'),
                'not_found' =>  __('No Help found'),
                'not_found_in_trash'  =>  __('No Help found in Trash')
            ),
            'public'  =>  true,
            'show_ui' =>  true,
            'publicy_queryable' =>  true,
            'exclude_from_search' =>  false,
            'menu_position' => 4,
//            'menu_icon' =>  get_stylesheet_directory_uri(). '/images/product.png',
            'hierarchical'  => false,
            'query_var' =>  true,
            'supports'  =>  array(
                'title', 'thumbnail'
            ),
            'rewrite' =>  array('slug'  =>  'help', 'with_front' =>  false),
//            'taxonomies' =>  array('post_tag', 'category'),
            'can_export'  =>  true,
//            'register_meta_box_cb'  =>  'call_to_function_do_something',
            'description' =>  __('Help description here.')
        )
    );
}

//add_action('init', 'create_help_taxonomies');
function  create_help_taxonomies(){
    register_taxonomy('help_cate', 'help', array(
        'hierarchical'  =>  true,
        'labels'  =>  array(
            'name'  =>  __('Categories'),
            'singular_name' =>  __('Categories'),
            'add_new' =>  __('Add New'),
            'add_new_item'  =>  __('Add New Categories'),
            'new_item'  =>  __('New Categories'),
            'search_items' =>  __('Search Categories'),
        ),
    ));
}


?>