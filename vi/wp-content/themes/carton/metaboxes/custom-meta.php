<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 4/4/13 9:31 AM
 * File: custom-meta.php 
 * Support : thangnm@lms.vn
 */

 global $wpalchemy_media_access; ?>
<div class="my_meta_control">

    <?php while($mb->have_fields_and_multi('docs')): ?>
        <?php $mb->the_group_open(); ?>
        <?php $mb->the_field('imgurl'); ?>
        <?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert'); ?>

        <p>
            <label>Image URL:</label><?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?><?php echo $wpalchemy_media_access->getButton(); ?>
            <?php $mb->the_field('imghover'); ?>
            <label>Hover URL:</label><input type="text" id="--><?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/><?php echo $wpalchemy_media_access->getButton(); ?>
            <br><a href="#" class="dodelete button">Remove</a>
        </p>
<hr>
<!--        --><?php //$mb->the_field('title'); ?>
<!--        <label for="--><?php //$mb->the_name(); ?><!--">Title</label>-->
<!--        <p><input type="text" id="--><?php //$mb->the_name(); ?><!--" name="--><?php //$mb->the_name(); ?><!--" value="--><?php //$mb->the_value(); ?><!--"/></p>-->

        <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>

    <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-docs button">Add</a><a href="#" class="dodelete-docs button">Remove All</a></p>

</div>