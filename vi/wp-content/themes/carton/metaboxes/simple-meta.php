<?php global $wpalchemy_media_access; ?>
<?php
function my_theme_add_editor_styles() {
    add_editor_style( 'content.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );
?>
<div class="my_meta_control">

    <p><a href="#" class="dodelete-docs button">Remove All</a></p>
    <?php while ($mb->have_fields_and_multi('docs')): ?>
        <?php $mb->the_group_open(); ?>
        <label>Title</label>
        <p>
            <?php $mb->the_field('title'); ?>
            <input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/>
        </p>

        <label>Description </label>
        <?php $mb->the_field('description');
        $settings = array(
            'textarea_rows' => '5',
            'media_buttons' => true,
            'tabindex' =>2,
            'quicktags' => false,
        );
        $val =  html_entity_decode($mb->get_the_value());
        $id = $mb->get_the_name();
        wp_editor($val,  $id , $settings );
        ?>

        <?php $mb->the_field('imgurl'); ?>
        <?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert'); ?>
        <p>
            <?php echo $wpalchemy_media_access->getButton(); ?>
            <?php echo $wpalchemy_media_access->getField(array('name' => $mb->get_the_name(), 'value' => $mb->get_the_value())); ?>

        </p>
        <a href="#" class="dodelete button">Remove</a>
        <?php $mb->the_group_close(); ?><hr>
    <?php endwhile; ?>

    <p style="margin-bottom:15px; padding-top:5px;"><a href="#" class="docopy-docs button">Add</a></p>

</div>