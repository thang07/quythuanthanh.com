<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 4/4/13 9:27 AM
 * File: media-spec.php 
 * Support : thangnm@lms.vn
 */

$custom_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_custom_meta',
    'title' => 'Product images',
    'types' => array('product'),
    'template' => get_stylesheet_directory() . '/metaboxes/custom-meta.php',
));

$tech_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_tech_meta',
    'title' => 'Tech images',
    'types' => array('technicality'),
    'template' => get_stylesheet_directory() . '/metaboxes/custom-meta.php',
));

$help_img_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_help_meta_img',
    'title' => 'Help Images',
    'types' => array('help'),
    'template' => get_stylesheet_directory() . '/metaboxes/custom-meta.php',
));


$help_child_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_help_meta_child',
    'title' => 'Help child',
    'types' => array('help'),
    'template' => get_stylesheet_directory() . '/metaboxes/simple-meta.php',
));


?>