<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 4/24/13 9:39 AM
 * File: page-618.php 
 * Support : thangnm@lms.vn
 */
get_header();?>
<div class="sitemap">
    <h1>Sitemap</h1>
    <hr>
    <div class="sitemapmenu">
        <?php $pages = get_pages(array('sort_order' => 'ASC','sort_column' => 'menu_order')); ?>
        <?php foreach ($pages as $page){?>
                <a href="<?php echo get_page_link( $page->ID); ?>"><?php echo $page->post_title; ?></a>
    <?php }?>
    </div>

    <div class="sitemapcontent">
        <h1>Sản Phẩm</h1>
        <ul>
                <?php global $post;
                $myposts = get_posts(array('post_type' => 'product', 'order' => 'ASC', 'orderby' => 'post_date'));
                foreach ($myposts as $post) : setup_postdata($post); ?>
                    <li>
                        <div class="item">
                            <a href="<?php the_permalink(); ?>">
                                <?php $terms = get_the_terms($post->ID, 'pro_cate');
                                foreach ($terms as $term) {
                                    echo $term->name;
                                }
                                ?>
                                <?php the_title();?>
                            </a>
                        </div>
                    </li>
                <?php endforeach;?>
        </ul>
    </div>

    <div class="sitemapcontent">
        <h1>Công Nghệ</h1>
        <ul>
                <?php global $post;
                $myposts = get_posts(array('post_type' => 'technicality', 'order' => 'ASC', 'orderby' => 'post_date'));
                foreach ($myposts as $post) : setup_postdata($post); ?>
                    <li>
                        <div class="item">
                            <a href="<?php the_permalink(); ?>">
                                <?php $terms = get_the_terms($post->ID, 'tech_cate');
                                foreach ($terms as $term) {
                                    echo $term->name;
                                }
                                ?>
                                <?php the_title();?>
                            </a>
                        </div>
                    </li>
                <?php endforeach;?>
        </ul>
    </div>

    <div class="sitemapcontent">
        <h1>Trợ Giúp</h1>
        <ul>
                <?php global $post;
                $myposts = get_posts(array('post_type' => 'help', 'order' => 'ASC', 'orderby' => 'post_date'));
                foreach ($myposts as $post) : setup_postdata($post); ?>
                    <li>
                        <div class="item">
                            <a href="<?php the_permalink(); ?>">
                                <?php $terms = get_the_terms($post->ID, 'help_cate');
                                if($terms){
                                foreach ($terms as $term) {
                                    echo $term->name;
                                }
                                }
                                ?>
                                <?php the_title();?>
                            </a>
                        </div>
                    </li>
                <?php endforeach;?>
        </ul>
    </div>

    <div class="sitemapcontent">
        <h1>Blog</h1>
        <ul>
                <?php global $post;
                $myposts = get_posts(array('order' => 'ASC', 'orderby' => 'post_date'));
                foreach ($myposts as $post) : setup_postdata($post); ?>
                    <li>
                        <div class="item">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title();?>
                            </a>
                        </div>
                    </li>
                <?php endforeach;?>
        </ul>
    </div>
</div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>