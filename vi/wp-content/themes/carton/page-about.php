<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:15 AM
 * File: page-about.php
 * Support : thangnm@lms.vn
 */
get_header();?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#footer > .bordercontent').hide();
        });
    </script>
    <!--begin html-->
        <div class="banner">
            <!--        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/banner.jpg">-->
            <?php if (function_exists('meteor_slideshow')) {
                meteor_slideshow();
            } ?>
            <div class="shadowbanner"></div>
        </div>
        <div class="bordercontent"></div>
        <div class="about">
            <?php the_content(); ?>
        </div>
    <!--end html-->

<?php get_footer(); ?>