<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/27/13 9:48 AM
 * File: header.php
 * Support : thangnm@lms.vn
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <title><?php wp_title('&#124;', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <?php if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>
    <?php wp_head(); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var url = window.location.href;
            $(".menu > li > a").each(function () {
                if (url == (this.href)) {
                    $(this).closest("li").addClass("active");
                }
            });
        });
    </script>

</head>

<body <?php body_class(); ?>>
<div id="header">
    <div class="logo">
        <a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a>
    </div>
    <div class="address">
        <div class="email">
            <img src="<?php echo get_template_directory_uri(); ?>/img/mail.png">

            <p class="customfont">
                Lô A-7-CN<br>
                Đường N6, KCN Mỹ Phước 1 <br>
                H. Bến Cát, T. Bình Dương
            </p>
        </div>
        <div class="tel" style="margin-top: 10px;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tel.png">

            <p class="customfont">
                0919773425<br>
                <i>Email:</i> info@quythuanthanh.com
            </p>
        </div>
    </div>
    <div class="language">
        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/LanguageVN.png"> Vietnamese</a>
        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/LanguageEN.png"> English</a>
    </div>
    <div class="navmenu">
        <?php wp_nav_menu(); ?>
    </div>

</div>


<div id="content">
    <div class="contentall">