<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:15 AM
 * File: content-product.php
 * Support : thangnm@lms.vn
 */

get_header();
?>


    <div class="product">
        <div class="productLeft">
            <ul>
                <li>Sản Phẩm</li>
                <?php
                global $post;
                $args = array('post_type' => 'product', 'order' => 'ASC', 'orderby' => 'post_date');
                $myposts = get_posts($args);
                foreach ($myposts as $post) : setup_postdata($post); ?>
                    <li>
                        <?php the_post_thumbnail(); ?>
                        <p>
                            <a href="<?php the_permalink(); ?>">
                                <?php $terms = get_the_terms($post->ID, 'pro_cate');
                                foreach ($terms as $term) {
                                    echo $term->name;
                                }
                                ?>
                                <br><span style="font-weight: bold"><?php the_title();?></span>
                            </a>
                        </p>
                    </li>
                    <?php
                    global $custom_mb;
                    $meta = $custom_mb->the_meta();
                    ?>
                <?php endforeach; ?>
                <hr>
                <li>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/skype.png">

                    <p>
                        <a> Skype</a>
                    </p>
                </li>
                <li>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/download.png">

                    <p>
                        <a href="#">Download<br>Template</a>
                    </p>
                </li>
                <li>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/upload.png">

                    <p>
                        <a href="#"> Upload <br>Files</a>
                    </p>
                </li>
            </ul>
        </div>
        <div class="productRight">
            <div class="banner">
                <?php if (function_exists('meteor_slideshow')) {
                    meteor_slideshow();
                } ?>
                <div class="shadowbanner"></div>
            </div>
            <div class="productRightBottom">
                <div class="productRightBottomTitle">
                    <p>
                        <a href="<?php bloginfo('wpurl'); ?>">Trang chủ</a>
                        / <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
                </div>
                <?php
                global $post;
                $args = array('post_type' => 'product', 'numberposts' => 1, 'offset' => 0, 'order' => 'ASC', 'orderby' => 'post_date');
                $myposts = get_posts($args);
                foreach ($myposts as $post) : setup_postdata($post);
                global $custom_mb;
                $meta = $custom_mb->the_meta();
                ?>

                <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/amazingslider.js"
                        type="text/javascript"></script>
                <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/initslider-1.js"
                        type="text/javascript"></script>

                <div class="productRightBottomContent">
                    <div class="boximgProduct">
                        <?php
                        foreach ($meta as $row) {
                        ?>
                        <div id="amazingslider-1">
                            <ul class="amazingslider-slides" style="display:none;">
                                <?php
                                foreach ($row as $img2) {
                                    ?>
                                    <li style="float: left">
                                        <a class="html5lightbox" title="" href=" <?php echo $img2['imghover']; ?>">
                                            <img alt="" src="<?php echo $img2['imgurl']; ?>">
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                            <ul class="amazingslider-thumbnails" style="display:none;">
                                <?php foreach ($row as $img) {
                                    ?>
                                    <li>
                                        <a class="html5lightbox" href="<?php echo $img['imghover']; ?>">
                                            <img src="<?php echo $img['imgurl']; ?>" alt="">
                                        </a>
                                    </li>
                                <?php
                                }
                                }
                                ?>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <div class="textProduct">
                <?php the_content(); ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>