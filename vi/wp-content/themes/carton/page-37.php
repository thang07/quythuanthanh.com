<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:15 AM
 * File: content-product.php
 * Support : thangnm@lms.vn
 */
get_header();
?>
    <div class="product">
        <div class="productLeft">
            <ul>
                <li>Sản Phẩm</li>
                <div style="margin-top: 9px">
                    <ul>
                        <?php
                        global $post;
                        $args = array('post_type' => 'product', 'order' => 'ASC', 'orderby' => 'post_date');
                        $myposts = get_posts($args);
                        foreach ($myposts as $post) : setup_postdata($post); ?>
                            <li>
                                <?php the_post_thumbnail(); ?>
                                <p>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php $terms = get_the_terms($post->ID, 'pro_cate');
                                        foreach ($terms as $term) {
                                            echo $term->name;
                                        }
                                        ?>
                                        <br><span style="font-family: conv_myriadpro-regular"><?php the_title();?></span>
                                    </a>
                                </p>
                            </li>
<!--                            --><?php
//                            print_r($myposts);
//                            ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <hr style="color: #5a1700; margin: 14px 0 11px">
                <div class="leftbottom">
                    <?php if (!dynamic_sidebar('sidebar')) : ?><?php endif; ?>
                </div>
            </ul>
        </div>
<!--        <div class="border"></div>-->
        <div class="productRight">
            <div class="banner">
                <?php if (function_exists('meteor_slideshow')) {
                    meteor_slideshow();
                } ?>
                <div class="shadowbanner"></div>
            </div>
            <div class="productRightBottom">
                <div class="productRightBottomTitle">
                    <p style="color: #6b0b0c">
                        <a href="<?php bloginfo('wpurl'); ?>">Trang chủ</a> / <a
                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a> / <a
                            href="<?php echo get_permalink($myposts[0]->ID); ?>">
                            <?php $terms = get_the_terms($myposts[0]->ID, 'pro_cate');
                            foreach ($terms as $term) {
                                echo $term->name;
                            }
                            ?>
                            <span style="text-transform: lowercase"><?php echo $myposts[0]->post_title;?></span>
                        </a>
                    </p>
                </div>
                <?php
                global $post;
                $args = array('post_type' => 'product', 'numberposts' => 1, 'offset' => 0, 'order' => 'ASC', 'orderby' => 'post_date');
                $myposts = get_posts($args);
                foreach ($myposts as $post) : setup_postdata($post);
                global $custom_mb;
                $meta = $custom_mb->the_meta();
                ?>

                <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/amazingslider.js"
                        type="text/javascript"></script>
                <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/initslider-1.js"
                        type="text/javascript"></script>

                <div class="productRightBottomContent">
                    <div class="boximgProduct">
                        <?php
                        foreach ($meta as $row) {
                        ?>
                        <div id="amazingslider-1" style="display:block;position:relative;">
                            <ul class="amazingslider-slides" style="display:none;">
                                <?php
                                foreach ($row as $img2) {
                                    ?>
                                    <li>
                                        <a class="html5lightbox" title=""
                                           href=" <?php echo $img2['imghover'] == '' ? $img2['imgurl'] : $img2['imghover']; ?>">
                                            <img alt="" src="<?php echo $img2['imgurl']; ?>">
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                            <ul class="amazingslider-thumbnails" style="display:none;">
                                <?php foreach ($row as $img) {
                                    ?>
                                    <li>
                                        <a class="html5lightbox" href="<?php echo $img['imghover']; ?>">
                                            <img src="<?php echo $img['imgurl']; ?>" alt="">
                                        </a>
                                    </li>
                                <?php
                                }
                                }
                                ?>

                            </ul>
                        </div>
                        <div class="clickview">
                            <p>Click to view large image</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="textProduct">
                <?php the_content(); ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>