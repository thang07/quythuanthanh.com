<?php
/*
Template Name: Carton
*/
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:23 AM
 * File: content-blog.php
 * Support : thangnm@lms.vn
 */

get_header();?>
    <div class="banner">
        <!--        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/banner.jpg">-->
        <?php if (function_exists('meteor_slideshow')) {
            meteor_slideshow();
        } ?>
        <div class="shadowbanner"></div>
    </div>
    <div class="blog">
        <div class="blogLeft">
            <h2> <?php
                $categories = get_categories(array('orderby'=>'id','order'=>'ASC'));
                echo '<a href="' . get_category_link($categories[0]->term_id) . '">' . $categories[0]->name . '</a>';
                ?>
            </h2>

            <div class="blogLeftContent">
                <div class="sublink">
                    <p style="color: #6b0b0c">
                        <a href="<?php bloginfo('wpurl'); ?>">Trang chủ</a> / <a
                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a> / <?php echo '<a href="' . get_category_link($categories[0]->term_id) . '">' . $categories[0]->name . '</a>';?>
                    </p>
                </div>
                <div class="blogLeftContentHeader">

                    <?php
                    $args = array('offset' => 0, 'category'=>$categories[0]->term_id);
                    $lastposts = get_posts($args);
                    foreach ($lastposts as $post) : setup_postdata($post); ?>
                        <article>
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h3>
                            <span class="linkaddress"
                                  style="text-transform: uppercase;float: left;margin: 0"><?php the_date(); ?></span>
                            <span class="linkaddress"><?php echo 'By ';
                                the_author();
                                the_category();
                                comments_number(' with 0 comment', 'with 1 comment', 'with % comment'); ?></span>
                            </br>
                            <!--                                --><?php //  the_content('Read more...'); ?>
                            <?php
                            $content2 = get_the_content();
//                            $content2 = preg_replace("/<span[^>]+\>/i", " ", $content2);
                            $pattern = '/(.*?\s){200}/';
                            preg_match($pattern, $content2, $matches);
                            $content2 = trim($matches[0]) . "";
                            echo $content2;
                            ?>
                           <br>
                            <a style="color: #82A53D;" class="readmore" href="<?php the_permalink(); ?>">Read more...</a>
                        </article>
                    <?php endforeach; ?>
<!--                        --><?php //endwhile; ?>
                </div>
            </div>
        </div>
        <div class="blogRight">

            <h2>Tin mới nhất</h2>

            <div class="blogRightContent">
                <div class="news">
                    <ul>
                        <?php
                        $args = array('offset' => 0);
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) : setup_postdata($post); ?>
                            <li class="newsnew">
                                <div style="float: left">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                                <div style="float: left; width: 135px">
                                    <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br>
                                        <span><?php the_date(); ?></span>
                                    </p>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="categories">
                    <h2>Categories</h2>
                    <?php
                    $categories = get_categories();
                    foreach ($categories as $category) {
                        echo '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
    <div class="arrow">
        <?php if (function_exists('wp_paginate')) {
            wp_paginate();
        } ?>
    </div>
<?php get_footer(); ?>