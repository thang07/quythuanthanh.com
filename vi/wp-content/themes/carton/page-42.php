<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:23 AM
 * File: page-help.php
 * Support : thangnm@lms.vn
 */


get_header();?>
    <div class="helppage">
        <div class="helppageLeft">
            <ul>
                <li>Trợ Giúp</li>
                <div style="margin-top: 9px">
                    <ul>
                        <?php
                        global $post;
                        $args = array('post_type' => 'help', 'order' => 'ASC', 'orderby' => 'post_date');
                        $myposts = get_posts($args);
                        foreach ($myposts as $post) : setup_postdata($post); ?>
                            <li>
                                <?php the_post_thumbnail(); ?>
                                <p>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php $terms = get_the_terms($post->ID, 'help_cate');
                                        if ($terms) {
                                            foreach ($terms as $term) {
                                                echo $term->name != null ? $term->name.'<br>' : "";
                                            }
                                        }
                                        ?>
                                        <span
                                            style="font-family: conv_myriadpro-regular"><?php the_title(); ?></span>
                                    </a>
                                </p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <hr style="color: #5a1700; margin: 14px 0 11px">
                <div class="leftbottom">
                    <?php if (!dynamic_sidebar('sidebar')) : ?><?php endif; ?>
                </div>
            </ul>
        </div>
        <div class="helppageRight">
            <div class="banner">
                <?php if (function_exists('meteor_slideshow')) {
                    meteor_slideshow();
                } ?>
                <div class="shadowbanner"></div>
            </div>
            <div class="helppageRightBottom">
                <div class="helppageRightBottomTitle">
                    <p style="color: #6b0b0c">
                        <a href="<?php bloginfo('wpurl'); ?>">Trang chủ</a> / <a
                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a> / <a
                            href="<?php echo get_permalink($myposts[0]->ID); ?>">
                            <?php $terms = get_the_terms($myposts[0]->ID, 'help_cate');
                            foreach ($terms as $term) {
                                echo $term->name;
                            }
                            ?>
                            <span><?php echo $myposts[0]->post_title; ?></span>
                        </a>
                    </p>
                </div>

                <?php
                global $post;
                $args = array('post_type' => 'help', 'numberposts' => 1, 'offset' => 0, 'order' => 'ASC', 'orderby' => 'post_date');
                $myposts = get_posts($args);
                foreach ($myposts as $post) :
                setup_postdata($post);
                global $help_img_mb;
                $meta = $help_img_mb->the_meta();
                if($meta){
                ?>
                <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/amazingslider.js"
                        type="text/javascript"></script>
                <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/initslider-1.js"
                        type="text/javascript"></script>
                <div class="helppageRightBottomContent">
                    <div class="boximgProduct">
                        <?php
                        foreach ($meta as $row) {
                        ?>
                        <div id="amazingslider-1" style="display:block;position:relative;">
                            <ul class="amazingslider-slides" style="display:none;">
                                <?php
                                foreach ($row as $img2) {
                                    ?>
                                    <li>
                                        <a class="html5lightbox" title=""
                                           href=" <?php echo $img2['imghover'] == '' ? $img2['imgurl'] : $img2['imghover']; ?>">
                                            <img alt="" src="<?php echo $img2['imgurl']; ?>">
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                            <ul class="amazingslider-thumbnails" style="display:none;">
                                <?php foreach ($row as $img) {
                                    ?>
                                    <li>
                                        <a class="html5lightbox" href="<?php echo $img['imghover']; ?>">
                                            <img src="<?php echo $img['imgurl']; ?>" alt="">
                                        </a>
                                    </li>
                                <?php
                                }
                                }
                                ?>

                            </ul>
                        </div>
                        <div class="clickview">
                            <p>Click to view large image</p>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php endforeach; ?>
                <div class="textHelp">
                    <?php the_content(); ?><br>
                    <?php
                    global $help_child_mb;
                    $meta = $help_child_mb->the_meta();
                    foreach ($meta as $row) {
                        foreach ($row as $data) {
                            ?>

                            <div class="imghelp">
                                <div class="boximghelp">
                                    <img src="<?php echo $data['imgurl'] ?>">

                                    <h3><?php echo $data['title'] ?></h3>
                                </div>
                                <div class="contentHelp">
                                    <?php echo $data['description'] ?>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>