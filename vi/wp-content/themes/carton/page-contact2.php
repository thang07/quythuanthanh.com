<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:24 AM
 * File: page-contact.php 
 * Support : thangnm@lms.vn
 */
get_header();?>
<div class="contentall" style="min-height: 563px;padding-top: 24px;">
    <div class="contact">
        <div class="contactleft">
            <div class="contactlefttop">
                <img src="<?php echo get_template_directory_uri(); ?>/img/mapContact.jpg">
            </div>
            <div class="contactleftbottom">
                <h2>Contact info</h2>
                <p>Lô A-7 Đường N6, KCN Mỹ Phước 1<br>
                    Huyện Bến Cát, Tỉnh Bình Dương<br>
                    Tp Hồ Chí Minh, Viet Nam</p>
                <table>
                    <tr>
                        <td>Telephone: </td>
                        <td>0650.3595368</td>
                    </tr>
                    <tr>
                        <td>Cellphone: </td>
                        <td>0919.773425</td>
                    </tr>
                    <tr>
                        <td>Fax: </td>
                        <td>0650.3595369</td>
                    </tr>
                    <tr>
                        <td>Email: </td>
                        <td>info@quythuanthanh.com</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="contactright">
            <p>Packaging is the science, art, and technology of enclosing or protecting products for distribution, storage, sale, and use. Packaging also refers to the process of design, evaluation, and production of packages. Packaging can be described as a coordinated system of preparing goods for transport, warehousing, logistics, sale, and end use. Packaging contains, protects, preserves, transports, informs, and sells.[1] In many countries it is fully integrated into government, business,</p>
            <h2>Feed Back</h2>
            <input type="text" placeholder="Name">
            <input type="text" placeholder="Email">
            <input type="text" placeholder="Phone">
            <textarea placeholder="Message"></textarea>
            <button type="submit" class="btn">Send</button>
            <button class="btn" type="reset" onclick="">Clear</button>
        </div>
    </div>
</div>

<?php get_footer(); ?>