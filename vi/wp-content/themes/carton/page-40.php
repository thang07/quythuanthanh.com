<?php/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:23 AM
 * File: content-technicality.php
 * Support : thangnm@lms.vn
 */
get_header();?>
    <div class="technical">
        <div class="technicalLeft">
            <ul>
                <li>Công Nghệ</li>
                <div style="margin-top: 9px">
                    <ul>
                        <?php
                        global $post;
                        $args = array('post_type' => 'technicality', 'order' => 'ASC', 'orderby' => 'post_date');
                        $myposts = get_posts($args);
                        foreach ($myposts as $post) : setup_postdata($post); ?>
                            <li>
                                <p>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php $terms = get_the_terms($post->ID, 'tech_cate');
                                        foreach ($terms as $term) {
                                            echo $term->name;
                                        }
                                        ?>
                                        <br> <span
                                            style="font-family: conv_myriadpro-regular"><?php the_title();?></span>
                                    </a>
                                </p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <hr style="color: #5a1700; margin: 14px 0 11px">
                <div class="leftbottom">
                    <?php if (!dynamic_sidebar('sidebar')) : ?><?php endif; ?>
                </div>
            </ul>
        </div>
        <div class="productRight">
            <div class="banner">
                <?php if (function_exists('meteor_slideshow')) {
                    meteor_slideshow();
                } ?>
                <div class="shadowbanner"></div>
            </div>
            <div class="productRightBottom">
                <div class="productRightBottomTitle">
                    <p style="color: #6b0b0c">
                        <a href="<?php bloginfo('wpurl'); ?>">Trang chủ</a> / <a
                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a> / <a
                            href="<?php echo get_permalink($myposts[0]->ID); ?>">
                            <?php $terms = get_the_terms($myposts[0]->ID, 'tech_cate');
                            foreach ($terms as $term) {
                                echo $term->name;
                            }
                            ?>
                            <span style="text-transform: lowercase"><?php echo $myposts[0]->post_title;?></span>
                        </a>
                    </p>
                </div>
                <?php
                global $post;
                $args = array('post_type' => 'technicality', 'numberposts' => 1, 'offset' => 0, 'order' => 'ASC', 'orderby' => 'post_date');
                $myposts = get_posts($args);
                foreach ($myposts as $post) : setup_postdata($post);
                ?>
                <div class="contenttechnical">
                    <?php the_content(); ?>
                    <!--<div class="contenttechnicalimg">
                        <?php /*the_post_thumbnail(); */?>
                    </div>-->
                    <!--<div class="contenttechnicaltext">
                        <?php /*the_content(); */?>
                    </div>-->
                </div>
            </div>

        </div>
            <?php endforeach; ?>
    </div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>