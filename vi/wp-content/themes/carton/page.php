<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 4/9/13 4:39 PM
 * File: page.php 
 * Support : thangnm@lms.vn
 */
get_header();
?>
<div style="min-height: 563px;padding-top: 24px;">
<?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
<?php endwhile; // end of the loop. ?>
        </div>
</div>
<?php get_footer(); ?>