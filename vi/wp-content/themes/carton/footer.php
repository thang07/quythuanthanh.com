<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/27/13 9:48 AM
 * File: footer.php
 * Support : thangnm@lms.vn
 */

?>

</div>
</div>
<div id="footer">
    <div class="bordercontent"></div>
    <div class="likepage">
        <a href="http://facebook.com" style="text-decoration: none">
            <img src="<?php echo get_template_directory_uri(); ?>/img/fb.jpg">
        </a>
        <a href="http://google.com" style="text-decoration: none">
            <img src="<?php echo get_template_directory_uri(); ?>/img/google.jpg"></a>
    </div>
    <div class="contentfooter">
        <ul class="contentfooterleft">
            <li>Sản Phẩm</li>
            <?php
            global $post;
            $args = array('post_type' => 'product', 'order' => 'ASC', 'orderby' => 'post_date');
            $myposts = get_posts($args);
            foreach ($myposts as $post) : setup_postdata($post); ?>
                <li>
                    <a href="<?php the_permalink(); ?>">
                        <?php $terms =  get_the_terms( $post->ID, 'pro_cate');
                        foreach ( $terms as $term ) {
                            echo $term->name;
                        }
                        ?>
                        <?php the_title();?>
                    </a>
                </li>
                <?php
                global $custom_mb;
                $meta = $custom_mb->the_meta();
                //print_r($meta);
                ?>
            <?php endforeach; ?>
            <?php  wp_reset_query(); ?>
        </ul>
        <ul class="contentfootercenter" style="width: 15%;">
            <li><a href="<?php echo get_site_url().'/about'; ?>">Giới Thiệu</a></li>
            <li><a href="<?php echo get_site_url().'/technicality'; ?>">Công Nghệ</a></li>
            <li><a href="<?php echo get_site_url().'/help'; ?>">Kiểu Thùng</a></li>
        </ul>
        <div class="contentfooterright">
            <p>Công ty bao bì Quý Thuận Thành chuyên<br>
                in ấn bao bì thùng carton in offset ở các<br>
                khu vực : in ấn Tp.hcm, in ấn Bình Dương...</p>
        </div>
        <div class="copyright">
            <p>Copyright &copy; 2013 <?php bloginfo('name'); ?>. All Right Reserved</p>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>