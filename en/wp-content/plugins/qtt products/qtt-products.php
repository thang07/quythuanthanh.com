<?php
/*
Plugin Name: QTT Product
Plugin URI: http://lms.vn
Description: Quản lý sản phẩm.
Version: 1.0
Author: thangnm@lms.vn
Author URI: mailto:thangnm@lms.vn
License: Free License
*/

require_once( plugin_dir_path( __FILE__ ) . 'functions.php');

/**
 *add address custom setting
 */
add_filter('admin_init', 'address_general_settings_register_fields');

function address_general_settings_register_fields()
{
    register_setting('general', 'my_address', 'esc_attr');
    add_settings_field('my_address', '<label for="my_address">'.__('Header Address: ' , 'my_field' ).'</label>' , 'address_general_settings_fields_html', 'general');
}

function address_general_settings_fields_html()
{
    $value = get_option( 'my_address', '' );
    echo '<textarea rows="4" cols="50" name="my_address" value="' . $value . '"></textarea>';
}

/**
 *add contact custom setting
 */
add_filter('admin_init', 'contact_general_settings_register_fields');

function contact_general_settings_register_fields()
{
    register_setting('general', 'my_contact', 'esc_attr');
    add_settings_field('my_contact', '<label for="my_contact">'.__('Header Contact' , 'my_field' ).'</label>' , 'contact_general_settings_fields_html', 'general');
}

function contact_general_settings_fields_html()
{
    $value = get_option( 'contact', '' );
    echo '<textarea rows="2" cols="50" name="my_contact" value="' . $value . '"></textarea>';
}


/**
 *add afooter custom setting
 */
add_filter('admin_init', 'footer_general_settings_register_fields');
function footer_general_settings_register_fields()
{
    register_setting('general', 'my_field', 'esc_attr');
    add_settings_field('my_footer', '<label for="my_footer">'.__('Footer info' , 'my_field' ).'</label>' , 'footer_general_settings_fields_html', 'general');
}

function footer_general_settings_fields_html()
{
    $value = get_option( 'my_footer', '' );
    echo '<textarea rows="4" cols="50" name="my_footer" value="' . $value . '" ></textarea>';
}



?>

