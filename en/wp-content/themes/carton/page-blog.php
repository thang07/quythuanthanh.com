<?php
/*
Template Name: Carton
*/
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:23 AM
 * File: content-blog.php
 * Support : thangnm@lms.vn
 */

get_header();?>
        <div class="banner">
            <!--        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/banner.jpg">-->
            <?php if (function_exists('meteor_slideshow')) {
                meteor_slideshow();
            } ?>
            <div class="shadowbanner"></div>
        </div>
        <div class="blog">
            <div class="blogLeft">
                <h2>Tin Tức</h2>

                <div class="blogLeftContent">
                    <div class="sublink">
                        <p><span style="color: #90acd4;"><a href="<?php bloginfo('wpurl');?>">Home page</a></span> /<span style="color: #90acd4;"><?php get_the_category();?></span>
                          <a href="<?php  the_permalink();?>"><?php the_title(); ?></a></p>
                    </div>
                    <div class="blogLeftContentHeader">
                        <article>
                            <?php
                            $args = array('numberposts' => 1, 'offset' => 0);
                            $lastposts = get_posts($args);
                            foreach ($lastposts as $post) : setup_postdata($post); ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                                <span style="text-transform: uppercase;float: left;margin: 0"><?php the_date(); ?></span>
                                <span><?php echo 'By ';the_author(); the_category();comments_number( ' with 0 comment', 'with 1 comment', 'with % comment' ); ?></span>

                                <?php the_content(); ?>
                            <?php endforeach; ?>
                        </article>
                    </div>
                </div>
            </div>
            <div class="blogRight">

                <h2>Tin mới nhất</h2>

                <div class="blogRightContent">
                    <div>
                        <ul>
                            <?php
                            $args = array('offset' => 1);
                            $lastposts = get_posts($args);
                            foreach ($lastposts as $post) : setup_postdata($post); ?>
                                <li class="newsnew">
                                    <div style="float: left">
                                        <?php the_post_thumbnail(); ?>
                                    </div>
                                    <div style="float: left; width: 135px">
                                        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br>
                                            <span style="text-transform: uppercase;"><?php the_date(); ?></span>
                                        </p>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="arrow">
<!--            <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/arrowBlog.png">-->
                <?php next_posts_link('<img src="'. get_template_directory_uri().'/img/arrowBlog.png">'); ?>
<!--                <span class="right">--><?php //previous_posts_link('Newer Posts &#8250;'); ?><!--</span>-->
        </div>
<?php get_footer(); ?>