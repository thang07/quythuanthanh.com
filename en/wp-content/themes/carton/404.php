<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/27/13 10:09 AM
 * File: 404.php 
 * Support : thangnm@lms.vn
 */
get_header();
?>
    <div class="error404">
        <div class="errorimg">
            <img src="<?php echo get_template_directory_uri(); ?>/img/img404.png">
        </div>
        <div class="errortext">
            <article>
                <h1 style="font-weight: bold;font-size: 80px;color: #82a53d; margin-bottom: 30px ">404</h1>
                <span style="font-size: 24px; color: #6b0b0c;">Uh-oh! We've lost you.</span>
                <p style="margin-top: 15px;color: #403e3e; font-size: 14px;line-height: 1.2;">
                    we cannot find the page that you're looking for. Is there a typo in the url? Try the search bar or
                    <span style="color: #82a53d;">contact support.</span>
                </p>
                <p style="margin-top: 30px; font-size: 14px;color: #6b0b0c;">Please <a style="color: #118acb;" href="<?php echo get_site_url(); ?>">click here</a> to return to our website</p>
            </article>
        </div>
    </div>
<?php get_footer(); ?>