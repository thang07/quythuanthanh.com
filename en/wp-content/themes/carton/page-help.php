<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:23 AM
 * File: page-help.php
 * Support : thangnm@lms.vn
 */

function wptuts_scripts_basic()
{
    wp_register_script('custom-script2', get_template_directory_uri() . '/js/script.js');
    wp_register_script('custom-script', get_template_directory_uri() . '/js/jquery.easing.js');
    wp_enqueue_script('custom-script');
    wp_enqueue_script('custom-script2');
}

add_action('wp_enqueue_scripts', 'wptuts_scripts_basic');

get_header();?>
    <script type="text/javascript">
        $(document).ready(function () {
            var buttons = { previous: $('.als-prev'),
                next: $('.als-next') };
            $('#jslidernews2').lofJSidernews({ interval: 2000,
                easing: 'easeInOutExpo',
                duration: 1200,
                auto: true,
                mainWidth: 300,
                mainHeight: 188,
                navigatorHeight: 60,
                navigatorWidth: 95,
                maxItemDisplay: 3,
                buttons: buttons });
        });

    </script>
        <div class="helppage">
            <div class="helppageLeft">
                <ul>
                    <li>Trợ Giúp</li>
                    <?php
                    global $post;
                    $args = array('post_type' => 'help', 'order' => 'ASC', 'orderby' => 'post_date');
                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post); ?>
                        <li>
                            <?php the_post_thumbnail(); ?>
                            <p>
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_title();?>
                                </a>
                            </p>
                        </li>
                        <?php
                        global $custom_mb;
                        $meta = $custom_mb->the_meta();
                        //print_r($meta);
                        ?>
                    <?php endforeach; ?>
                    <hr>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/skype.png">

                        <p>
                            <a> Skype</a>
                        </p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/download.png">

                        <p>
                            <a href="#">Download<br>Template</a>
                        </p>
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/upload.png">

                        <p>
                            <a href="#"> Upload <br>Files</a>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="helppageRight">
                <div class="banner">
                    <!--        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/banner.jpg">-->
                    <?php if (function_exists('meteor_slideshow')) {
                        meteor_slideshow();
                    } ?>
                    <div class="shadowbanner"></div>
                </div>
                <div class="helppageRightBottom">
                    <div class="helppageRightBottomTitle">
                        <p style="color: #6b0b0c;font-style: italic;padding: 5px 0 4px 30px;">Home page / Support</p>
                    </div>

                    <?php
                    global $post;
                    $args = array('post_type' => 'help', 'numberposts' => 1, 'offset' => 0, 'order' => 'ASC', 'orderby' => 'post_date');
                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post);
                    global $help_img_mb;
                    $meta = $help_img_mb->the_meta();
                    ?>

                    <div class="helppageRightBottomContent">
                        <div id="jslidernews2" class="boximgProduct lof-slidecontent">
                            <?php
                            foreach ($meta as $row) {
                            ?>
                            <div class="main-slider-content preload"
                                 style="width: 303px;height: 190px; float: left;overflow: hidden;position: absolute; ">
                                <ul class="sliders-wrap-inner" style="overflow: hidden;position: absolute">
                                    <?php
                                    foreach ($row as $img2) {
                                        ?>
                                        <li style="float: left"><img src="<?php echo $img2['imgurl']; ?>"></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="snavigator-content" style="position: relative; left: 300px">
                                <div class="boximgProductLi navigator-wrapper"
                                     style="overflow: hidden;position: relative;padding-left: 15px;">
                                <span class="als-prev"> <img src="<?php echo get_template_directory_uri(); ?>/img/arrowProduct1.png" alt="prev" title="previous"/>
                                 </span>

                                    <div class="navigator-wrap-inner" style="height: 197px; overflow: hidden">
                                        <ul style="position: relative; padding-left: 5px">
                                            <?php
                                            foreach ($row as $img) {
                                                ?>
                                                <li><img style="width: 95px;height: 60px"
                                                         src="<?php echo $img['imgurl']; ?>"></li>
                                            <?php
                                            }
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                <span class="als-next"><img src="<?php echo get_template_directory_uri(); ?>/img/arrowProduct2.png" alt="next" title="next"/></span>

                                </div>
                            </div>
                        </div>
                        <div class="textHelp">
                            <?php
                            global $help_child_mb;
                            $meta = $help_child_mb->the_meta();
                            foreach ($meta as $row) {
                                foreach ($row as $data) {
                                    ?>

                                    <div class="imghelp">
                                        <img src="<?php echo $data['imgurl'] ?>">

                                        <div class="contentHelp">
                                            <h3><?php echo $data['title']?></h3>
                                            <?php echo $data['description']?>
                                        </div>
                                    </div>
                                <?php
                                }
                            }
                            ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>