<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/29/13 10:23 AM
 * File: content-technicality.php
 * Support : thangnm@lms.vn
 */
function wptuts_scripts_basic()
{
    wp_register_script('custom-script2', get_template_directory_uri() . '/js/script.js');
    wp_register_script('custom-script', get_template_directory_uri() . '/js/jquery.easing.js');
    wp_enqueue_script('custom-script');
    wp_enqueue_script('custom-script2');
}

add_action('wp_enqueue_scripts', 'wptuts_scripts_basic');
get_header();?>
    <script type="text/javascript">
        $(document).ready(function () {
            var buttons = { previous: $('.als-prev'),
                next: $('.als-next') };
            $('#jslidernews2').lofJSidernews({ interval: 5000,
                easing: 'easeInOutExpo',
                duration: 1200,
                auto: true,
                mainWidth: 300,
                mainHeight: 188,
                navigatorHeight: 60,
                navigatorWidth: 95,
                maxItemDisplay: 3,
                buttons: buttons });
        });

    </script>

        <div class="technical">
            <div class="technicalLeft">
                <ul>
                    <li>Công Nghệ</li>
                    <?php
                    global $post;
                    $args = array('post_type' => 'technicality', 'order' => 'ASC', 'orderby' => 'post_date');
                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post); ?>
                        <li>
                            <?php the_post_thumbnail(); ?>
                            <p>
                                <a style="width: 146px" href="<?php the_permalink(); ?>">
                                    <?php $terms = get_the_terms($post->ID, 'tech_cate');
                                    foreach ($terms as $term) {
                                        echo $term->name;
                                    }
                                    ?> <?php the_title();?>
                                </a>
                            </p>
                        </li>
                        <?php
                        global $tech_mb;
                        $meta = $tech_mb->the_meta();
                        //print_r($meta);
                        ?>
                    <?php endforeach; ?>
                    <hr>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/skype.png">

                        <p>
                            <a> Skype</a>
                        </p>
                    </li><li>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/download.png">

                        <p>
                            <a href="#">Download<br>Template</a>
                        </p>
                    </li><li>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/upload.png">

                        <p>
                            <a href="#" > Upload <br>Files</a>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="productRight">
                <div class="banner">
                    <?php if (function_exists('meteor_slideshow')) {
                        meteor_slideshow();
                    } ?>
                    <div class="shadowbanner"></div>
                </div>
                <div class="productRightBottom">
                    <div class="productRightBottomTitle">
                        <p style="color: #6b0b0c;font-style: italic;padding: 5px 0 4px 30px;"><a
                                href="<?php bloginfo('wpurl'); ?>">Home page</a>
                            / <?php the_title(); ?></p>
                    </div>
                    <?php
                    while (have_posts()) : the_post();
                        global $tech_mb;
                        $meta = $tech_mb->the_meta();
                        ?>
                        <div class="productRightBottomContent">
                            <div id="jslidernews2" class="boximgProduct">
                                <?php
                                foreach ($meta as $row) {
                                ?>
                                <div class="main-slider-content preload" style="width: 303px;height: 190px; float: left;overflow: hidden;position: absolute; ">
                                    <ul class="sliders-wrap-inner" style="overflow: hidden;position: absolute">
                                        <?php
                                        foreach ($row as $img2) {
                                            ?>
                                            <li style="float: left"><img src="<?php echo $img2['imgurl']; ?>"></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                    <div class="snavigator-content" style="position: relative; left: 300px">
                                        <div class="boximgProductLi navigator-wrapper" style="overflow: hidden;position: relative;padding-left: 15px;">
                                            <span class="als-prev">
                                             <img src="<?php echo get_template_directory_uri(); ?>/img/arrowProduct1.png" alt="prev" title="previous"/>
                                            </span>
                                            <div class="navigator-wrap-inner" style="height: 197px; overflow: hidden">
                                                <ul style="position: relative; padding-left: 5px">
                                                    <?php foreach ($row as $img) {
                                                        ?>
                                                        <li><img style="width: 95px;height: 60px" src="<?php echo $img['imgurl']; ?>"></li>
                                                    <?php
                                                    }
                                                    }
                                                    ?>

                                                </ul>
                                            </div>
                                            <span class="als-next">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/arrowProduct2.png" alt="next" title="next"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="textProduct">
                            <?php the_content(); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>


<?php  wp_reset_query(); ?>
<?php get_footer(); ?>