<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/27/13 9:45 AM
 * File: index.php
 * Support : thangnm@lms.vn
 */
get_header();
?>

        <div class="banner">
            <!--        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/banner.jpg">-->
            <?php if (function_exists('meteor_slideshow')) {
                meteor_slideshow();
            } ?>
            <div class="shadowbanner"></div>
        </div>
        <div class="bordercontent"></div>
        <div class="infoIndex">
            <div class="infoIndexLeft">
                <?php $content_post = get_post(18);?>
                <?php $content = $content_post->post_content;?>
                <?php $content = apply_filters('the_content', $content);?>
                <?php
                $pattern = '/(.*?\s){160}/';
                preg_match($pattern, $content, $matches);
                $content = trim($matches[0]) . "...<br>";
                ?>
                <?php echo $content; ?>
                <a class="readmore" style="float: right" href="<?php echo get_permalink(18); ?>">Read more...</a>
            </div>
            <div class="infoIndexRight">
                <ul>
                    <?php
                    global $post;
                    $args = array('post_type' => 'product', 'order' => 'ASC', 'orderby' => 'post_date');
                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post); ?>
                        <li>
                            <?php the_post_thumbnail(); ?>
                            <!--  <a href="--><?php //the_permalink(); ?><!--">-->
                            <?php //the_title(); ?><!--</a>-->
                            <p>
                                <a href="<?php the_permalink(); ?>">
                                    <span style="font-weight: bold"><?php the_title();?></span><br>
                                    <?php $terms =  get_the_terms( $post->ID, 'pro_cate');
                                    foreach ( $terms as $term ) {
                                        echo $term->name;
                                    }
                                    ?>

                                </a>
                            </p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="blogIndex">
            <div class="infoIndexLeft">
                <h2>Blog:</h2>
                <article>
                    <?php
                    $args = array('numberposts' => 1, 'offset' => 0);
                    $lastposts = get_posts($args);
                    foreach ($lastposts as $post) : setup_postdata($post); ?>
                        <h3 style="margin: 5px 0;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br>
                        </h3>
                        <?php
                        $content2 = the_content();
                        $pattern = '/(.*?\s){50}/';
                        preg_match($pattern, $content2, $matches);
                        $content2 = trim($matches[0]) . "";
                        echo $content2;
                        ?>
                        <a class="readmore" href="<?php the_permalink(); ?>">(Read more...)</a>
                    <?php endforeach; ?>
                </article>
            </div>
            <div class="infoIndexRight">
                <h2>Video</h2>
                <img src="<?php echo get_template_directory_uri(); ?>/img/video.jpg">
            </div>
        </div>


<?php  wp_reset_query(); ?>

<?php get_footer(); ?>