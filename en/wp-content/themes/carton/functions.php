<?php
/**
 * Created by JetBrains PhpStorm.
 * User: thangnm
 * Date: 3/27/13 9:52 AM
 * File: functions.php
 * Support : thangnm@lms.vn
 */


function carton_setup()
{
    add_editor_style();
    register_nav_menu('primary', __('Primary Menu', 'carton'));
}

add_action('after_setup_theme', 'carton_setup');


/**
 *Add sidebar for template
 */
function carton_widgets_init()
{

    register_sidebars(1, array(
        'name' => 'sidebar',
        'id' => 'widget',
        'description' => 'Widgets in this area will be shown below the the content of every page.',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',));
}

add_action('widgets_init', 'carton_widgets_init');



include_once 'metaboxes/setup.php';

include_once 'metaboxes/custom-spec.php';


?>